import $ from 'jquery'
import Handlebars from 'handlebars'
import {HuffmanAlgo} from './Huffman'
import  {CodeSystem} from './CodeSystem'



$('#calculate').click(() => {

  let input_text = $("#input").val().replace(/(\r\n|\n|\r)/gm,'');
  const huffman = new HuffmanAlgo(input_text);
  let result = huffman.getResult();

  let cds = new CodeSystem(result);


  let context = {
    data : result,
    entropy: huffman.calculateEntropy(),
    cost: huffman.calculateCost(),
    best_code: cds.getBestCodeSystem()
  };

  let theTemplateScript = $("#table-template").html();
  let theTemplate = Handlebars.compile(theTemplateScript);
  let theCompiledHtml = theTemplate(context);
  $('#table').empty().append(theCompiledHtml);

});