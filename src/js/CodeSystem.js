

export class CodeSystem {

  constructor(alphabet) {
    this.alphabet = alphabet;
    this.alphabet_string = '';
    this.sub_alphabet_array = [];

    for(let item of this.alphabet) {
      this.alphabet_string += item.symbol;
    }

    this.getResult();
  }

  getResult() {

    this.initialSubAlphabets();

    for(let k=1; k<this.alphabet.length; k++) {

      let steps = this.alphabet.length - k;

      for(let j = 0; j < steps; j++) {

        let string = this.alphabet_string.substring(j, k+1 + j);
        let splitting = [];

        for(let splits=0; splits < k; splits++) {

          let total_cost = 0;
          let left = string.substr(0, splits + 1);
          let right = string.substr(splits + 1, string.length);
          let left_find = this.findByString(left);
          let right_find = this.findByString(right);

          total_cost += left_find.cost + right_find.cost + this.getUniqueSymbolsCost(string);

          let left_code = this.addCodePart(left_find, '0');
          let right_code = this.addCodePart(right_find, '1');
          let code = left_code.concat(right_code);

          splitting.push({
            symbol: string,
            cost: total_cost,
            code: code
          });
        }

        let min_code = splitting.reduce((prev, curr)=> {
          return prev.cost < curr.cost ? prev: curr;
        });

        this.sub_alphabet_array.push(min_code);
      }
    }
  }

  getUniqueSymbolsCost(original_string) {

    let cost = 0;
    let unique = original_string.split('').filter( (item, i, ar)=> {
      return ar.indexOf(item) === i;
    }).join('');

    for(let unique_symbol of unique) {

      let alph_object = this.alphabet.filter(
        (obj)=> {
          return obj.symbol == unique_symbol
        });

      cost += alph_object[0].possibility;

    }

    return cost;
  }

  findByString(substring) {
    return this.sub_alphabet_array.filter( (obj)=> {
      return obj.symbol == substring
    })[0];
  }

  initialSubAlphabets() {

    for(let item of Object.keys(this.alphabet)) {

      this.sub_alphabet_array.push({
        symbol: this.alphabet[item].symbol,
        cost: 0,
        code: []
      });

    }

  }

  addCodePart(item, code) {

    if(item.code.length == 0) {
      return [code];
    }

    let code_array = [];

    for(let sub_item of item.code) {
      code_array.push(code.concat(sub_item));
    }

    return code_array;
  }


  getBestCodeSystem() {
    return this.sub_alphabet_array[this.sub_alphabet_array.length - 1].code;
  }

}