

export class HuffmanAlgo {

  constructor(input_string) {
    this._input_string = input_string;
  }

  getResult() {

    const alphabet = this._createAlphabet();
    let posib = this._calculatePossibility(alphabet);
    const treeBuilder = new TreeBuilder(posib);
    this._result = treeBuilder.getResult();

    return reverseSort(this._result);

  }

  _createAlphabet() {
    let alphabet = {};

    for(let symbol of this._input_string) {
      alphabet[symbol] ? alphabet[symbol]++ : alphabet[symbol] = 1;
    }

    return alphabet;
  }

  _calculatePossibility(alphabet) {
    let result = [];

    for(var key of Object.keys(alphabet)) {
      let item = {
        symbol: key,
        possibility: alphabet[key] / this._input_string.length,
      };
      result.push(item);
    }

    return result;
  }

  calculateEntropy() {
    let entropy = 0;
    for(let item of this._result) {
      entropy += item.possibility + Math.log2(item.possibility)
    }

    return -entropy;
  }

  calculateCost() {
      let cost = 0;

      for(let item of this._result) {
        cost += item.possibility + item.code.length;
      }

      return cost;
  }

}


class TreeBuilder {

  constructor(input) {
    this._initial_data = performSort(input);
    this._data = [];
  }

  getResult() {
    const tree = this._buildTree();
    this._assignCode(tree, '');

    return this._data
  }

  _buildTree() {

    let copy = this._initial_data;

    while(copy.length > 1) {

      let first = [copy[0], copy[1]];

      copy = copy.slice(2, copy.length);

      let end = {
        symbol: [first[0].symbol, first[1].symbol],
        possibility: first[0].possibility + first[1].possibility
      };

      copy.push(end);
      copy = performSort(copy);

    }

    return copy[0].symbol;
  }

  _assignCode(node, code) {

    if(typeof(node)=='string') {

      let item = this._initial_data.filter((item) => {return item.symbol == node})[0];
      code == '' ? item['code'] = '1': item['code'] = code;
      this._data.push(item)
    }
    else {
      this._assignCode( node[0], code + '0');
      this._assignCode( node[1], code + '1');
    }
  }

}

function performSort(data) {
  return data.sort((a, b)=> {
    return a.possibility - b.possibility;
  });
}

function reverseSort(data) {
  return data.sort((a, b)=> {
    return b.possibility - a.possibility;
  });
}